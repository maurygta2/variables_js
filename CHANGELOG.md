# 19/05/2019
* 🚫*New Error logs*
* ➕*Update Methods(add,div,mul,div)*
* ✔*Add this.clearChild - variable()*
* ✔*Add this.hasChild - variable()*
* ✔ *Add this.getAllChild - variables()*

# 18/05/2019
* ❌ *Bugs fixed : Too many errors and console.error* 
* 🚫 *New Errors logs*
* ✔ *Add this.isString - variable()*
* ✅ *Rename this.ischild is this.isStructure - variable()*
* ✔ *Add this.isNumber - variable()*

# 16/05/2019
*  ✔ *Add this.lengthchild - variable()* 
*  ✔ *Add this.addChild - variables()*
*  ✔ *Add this.deleteChild - variables()*
*  ✔ *Add this.getchild - variables()*