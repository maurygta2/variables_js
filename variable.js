/*
** @autor Maury
** @language L++
** @project Variable_js
** @version 1.0.0
*/


/*jshint maxerr: 10000000 */
// Variable create
function variable(varData) {
    this.name = varData.name;
    if (varData.value === undefined) {
        this.isStructure = true;
        this.child = {};
    } else {
        this.value = varData.value;
        this.isStructure = false;
    }
    this.length = function() {
        if (!this.isStructure) {
            let f1 = String(this.value);
            return f1.length;
        } else {
            console.error('Error #14');
            return "";
        }
    };
    this.lengthChild = function(name) {
        if (this.isStructure){
            let f1 = String(this.child[name]);
            return f1.length;
        } else {
            console.error("Error #11");
            return "";
        }
    };
    this.isNumber = function() {
        if (!this.isStructure) {
            let f1 = Number(this.value);
            if (f1 !== this.value) {
                return false;
            } else {
                return true;
            }
        } else {
            console.error('Error #13');
            return "";
        }
    };
    this.isString = function() {
        if (!this.isStructure) {
            let f1 = String(this.value);
            if (f1 !== this.value) {
                return false;
            } else {
                return true;
            }
        } else {
            console.error("Error #12");
            return "";
        }
    };
    this.clearChild = function() {
        if (this.isStructure) {
            this.child = {};
        } else {
            console.error("Error #15");
        }
    };
    this.hasChild = function(child) {
        if (child !== undefined && child !== "" && (typeof(child) == "number" || typeof(child) == "string" && this.isStructure)) {
            if (this.child[child] !== undefined) {
                return true;
            } else {
                return false;
            }
        } else {
            console.error("Error #17");
            return "";
        }
    };
    this.add = function(add,child) {
        if (!this.isStructure && child !== undefined) {
            console.error("Error #2.2");
            return;
        }
        if (add !== undefined && add !== "" && typeof(add) == 'number' || typeof(add) == "string") {
            if (!this.isStructure) {
                this.value += add;
            } else if (this.child[child] !== undefined && this.isStructure && child !== undefined && child !== "" &&(typeof(child) == 'number') || typeof(child) == "string") {
                this.child[child] += add;
            } else {
                console.error("Error #2.1");
            }
        } else {
            console.error("Error #2");
        }
    };
    this.sub = function(sub,child) {
        if (!this.isStructure && child !== undefined) {
            console.error("Error #3.4");
            return;
        }
        if (sub !== undefined && sub !== "" &&(typeof(sub) == "number" || typeof(sub) == "string")) {
            if (!this.isStructure) {
                let f1 = sub - this.value;
                if (!isNaN(f1)) {
                    this.value -= sub;
                } else {
                    console.error('Error #3.3');
                }
            } else if (this.child[child] !== undefined && this.isStructure && child !== undefined && child !== "" &&(typeof(child) == "number" || typeof(child) == "string")) {
                let f2 = sub - this.child[child];
                if (!isNaN(f2)) {
                    this.child[child] -= sub;
                } else {
                    console.error("Error #3.2");
                }
            } else {
                console.error("Error #3.1");
            }
        } else {
            console.error("Error #3");
        }
    };
    this.mul = function(mul,child) {
        if (!this.isStructure && child !== undefined) {
            console.error("Error #4.4");
            return;
        }
        if (mul !== undefined && mul !== "" && (typeof(mul) == "number" || typeof(mul) == "string")) {
            if (!this.isStructure) {
                let f1 = mul * this.value;
                if (!isNaN(f1)) {
                    this.value *= mul;
                } else {
                    console.error('Error #4.3');
                }
            } else if (this.child[child] !== undefined && this.isStructure && child !== undefined && child !== "" &&(typeof(child) == "number" || typeof(child) == "string")) {
                let f2 = mul * this.child[child];
                if (!isNaN(f2)) {
                    this.child[child] *= mul;
                } else {
                    console.error("Error #4.2");
                }
            } else {
                console.error("Error #4.1");
            }
        } else {
            console.log("Error #4");
        }
    };
    this.div = function(div,child) {
        if (!this.isStructure && child !== undefined) {
            console.error("Error #5.4");
            return;
        }
        if (div !== undefined && div !== "" &&(typeof(div) == 'number' || typeof(div) == "string")) {
            if (!this.isStructure) {
                let f1 = div / this.value;
                if (!isNaN(f1) && div !== undefined && div !== "") {
                    this.value /= div;
                } else {
                    console.error('Error #5.3');
                }
            } else if (this.child[child] !== undefined && this.isStructure && child !== undefined && child !== "" &&(typeof(child) == "number" || typeof(child) == "string")) {
                let f2 = div / this.child[child];
                if (!isNaN(f2)) {
                    this.child[child] /= div;
                } else {
                    console.error('Error #5.2');
                }
            } else {
                console.error('Error #5.1');
            }
        } else {
            console.error("Error #5");
        }
    };
}


// Storage Variable
function variables() {
    this.length = 0;
    this.getvariable = {};
    this.addVar = function(name,value) {
        if (this.getvariable[name] === undefined && (typeof(name) == 'string' || typeof(name) == 'number') && (name !== "" && name !== undefined) && (typeof(value) == "number" || typeof(value) == "string") && (value !== "" && value !== undefined)) {
            this.getvariable[name] = new variable({
                name: name,
                value: value
            });
            this.length += 1;
        } else {
            console.error("Error #1");
        }
    };
    this.addChild = function(name,child,value) {
        if ((typeof(name) == 'string' || typeof(name) == 'number') && (name !== "" && name !== undefined) && (typeof(value) == "number" || typeof(value) == "string") && (value !== "" && value !== undefined)) {
            if (this.getvariable[name] === undefined && (typeof(child) == 'number' || typeof(child) == "string") && (child !== undefined && child !== "")) {
                this.getvariable[name] = new variable({
                    name: name
                });
                this.getvariable[name].child[child] = value;
                this.length += 1;
            } else if (this.getvariable[name] !== undefined && this.getvariable[name].ischild && this.getvariable[name].child[child] === undefined && (typeof(child) == 'number' || typeof(child) == 'string') && (child !== "" && child !== undefined)) {
                this.getvariable[name].child[child] = value;
            } else {
                console.error("Error #8");
            }
        }
    };
    this.deleteVar = function(name) {
        if (this.getvariable[name] !== undefined && (typeof(name) == "string" || typeof(name) == "number") && (name !== undefined && name !== "")) {
            delete this.getvariable[name];
            this.length -= 1;
        } else {
            console.error("Error #6");
        }
    };
    this.deleteChild = function(name,child) {
        if (this.getvariable[name] !== undefined && name !== undefined && name !== "" && (typeof(name) == "string" || typeof(name) == "number") && child !== "" && child !== undefined && (typeof(child) == "string" || typeof(child) == "number")) {
            if (this.getvariable[name].isStructure) {
                if (this.getvariable[name].child[child] !== undefined) {
                    delete this.getvariable[name].child[child];
                } else {
                    console.error("Error #9.2");
                }
            } else {
                console.error("Error #9.1");
            }
        } else {
            console.error("Error #9");
        }
    };
    this.getVar = function(name) {
        if (this.getvariable[name] !== undefined && name !== undefined && name !== "" &&(typeof(name) == "number") || typeof(name) == "string"){
            if(!this.getvariable[name].isStructure) {
                return this.getvariable[name].value;
            } else {
                console.error("Error #7.1");
                return "";
            }
        } else {
            console.error("Error #7");
            return "";
        }
    };
    this.getChild = function(name,child) {
        if (this.getvariable[name] !== undefined && child !== undefined && child !== "" &&(typeof(child) == "number" || typeof(child) == "string") && name !== "" && name !== undefined &&(typeof(name) == "string" || typeof(name) == 'number')) {
            if (this.getvariable[name].isStructure) {
                if (this.getvariable[name].child[child] !== undefined) {
                    return this.getvariable[name].child[child];
                } else {
                    console.error("Error #10.2");
                    return "";
                }
            } else {
                console.error('Error #10.1');
                return "";
            }
        } else {
            console.error('Error #10');
            return "";
        }
    };
    this.getAllChild = function(name) {
        if (this.getvariable[name] && name !== "" && name !== undefined &&(typeof(name) == "number" || typeof(name) == "string")) {
            if (this.getvariable[name].isStructure) {
                return this.getvariable[name].child;
            } else {
                console.error("Error 16.1");
            }
        } else {
            console.error("Error #16");
        }
    };
}
// your storage variables
var myStorage = new variables();