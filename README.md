# **How Use Variable_JS** 😎
1. Create script tag html. USE src 🎈 

```html
<script src="https://cdn.statically.io/gl/maurygta2/variables_js/master/variable.js">
</script>
```
2. Some basic variable_js commands are: 🎉 

```javascript
myStorage.addVar("myvar",3); // add variable
myStorage.deleteVar("myvar"); // remove var
myStorage.getVar("myvar"); // return variable value
```
*Read file variable.js*

# Methods variable() 📌 
| Method | Example |
| ------ | ------ |
| this.div(param) | this.div(7); this.div(2,"mychild"); value / div |
| this.mul(param) | this.mul(2); this.mul(23,'mychild'); value * mul |
| this.name | this.name = 1; return 1 |
| this.isStructure | this.isStructure; return true or false |
| this.child | this.child = {}; return {} |
| this.value | this.value = 24; return 24 |
| this.length() | this.value = 234; return 3 |
| this.lengthChild(param) | this.child.mychild = 12; return 2 |
| this.add(param,param2) | this.add(2); this.add(3,"mychild"); value + add |
| this.sub(param,param2) | this.sub(11); this.sub(2,'mychild'); value - sub |
| this.isNumber() | this.value = 2; return true; |
| this.isString() | this.value = "w"; return true; |
| this.hasChild(param) | this.hasChild("mychild"); return true or false |
| this.clearChild()| this.clearChild(); remove all child |
## Using variable() method ✔ 
```javascript
myStorage.getvariable.myvar.mul(4); // value * 4
myStorage.getvariable.myvar.div(7); // value / 7
```
# Methods variables() 📌 
| Method | Exampe |
| ------ | ------ |
| this.length | this.length; return number of existing variables |
| this.getvariable | this.getvariable; return all variables |
| this.addVar(param,param2) | this.addVar("myvar","value");  |
| this.addChild(param,param2,param3) | this.addChild("myvar","mychild","value"); |
| this.deleteVar(param) | this.deleteVar("myvar"); |
| this.deleteChild(param,param2) | this.deleteChild("myvar","mychild"); |
| this.getVar(param) | this.getVar("myvar"); return variable value; |
| this.getChild(param,param2) | this.getChild("myvar","mychild"); return variable child value |
| this.getAllChild(param) | this.getAllChild("mychild"); return all children|
## Using variables() method ✔ 
```javascript
myStorage.length; // return var number existing
myStorage.getvariable; // return all variables
myStorage.getVar("myvar"); // return var value
```
# Message Error ❌
* Error #1: Create Variable Error
* Error #2: Add Value Error
* Error #3: Sub Value Error
* Error #4: Mul Value Error
* Error #5: Div Value Error
* Error #6: Delete Var Error
* Error #7: Get Var Error
* Error #8: Add Child Error
* Error #9: Delete Child Error
* Error #10: Get Child Error
* Error #11: Length Child Error
* Error #12: Is String Error
* Error #13: Is Number Error
* Error #14: Variable() Length Error
* Error #15: Clear Child Error
* Error #16: Get All Children Error
* Error #17: Has Child Error